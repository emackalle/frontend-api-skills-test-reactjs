
import "./styles/default.scss";

const show = () => {
  let oktoLoader = document.getElementById("oktoLoader");
  if(oktoLoader){
    let counter = oktoLoader.getAttribute("data-counter")*1;
    oktoLoader.setAttribute("data-counter", counter+1);
  }else{
    let loaderContainer = document.createElement("div");
    let loaderWrapper = document.createElement("div");
    for(let i=0; i<9; i++){
      loaderWrapper.appendChild(document.createElement("span"));
    }
    loaderWrapper.classList.add("okto-loader-wrapper");
    loaderContainer.appendChild(loaderWrapper);
    loaderContainer.classList.add("okto-loader");
    loaderContainer.id = "oktoLoader";
    loaderContainer.setAttribute("data-counter", 1);
    document.getElementsByTagName("body")[0].appendChild(loaderContainer)      
  }
}

const hide = () => {
  let oktoLoader = document.getElementById("oktoLoader");
  if(oktoLoader){
    let counter = oktoLoader.getAttribute("data-counter")*1;
    counter === 1 ? oktoLoader.remove() : oktoLoader.setAttribute("data-counter", counter-1);
  }
}

export default { show, hide };