import axios from 'axios';

import config from './config';

import loader from "./loader";

// SET BASE URL
axios.defaults.baseURL = config.BASE_URL;

// SET HEADERS
axios.defaults.headers["Content-Type"] = "text/plain;charset=UTF-8";

// INTECEPTORS - SET AUTHORIZATION ON HEADERS
axios.interceptors.request.use(async function({ withLoader=true, ...config }){
    if(withLoader){
        loader.show(); // SHOW THE LOADER WHEN `withLoader` IS TRUE
    }
    return config
});

axios.interceptors.response.use(response => {
    loader.hide(); // HIDE THE LOADER AFTER REQUEST
    return response;
}, function (error){
    loader.hide(); // HIDE THE LOADER AFTER REQUEST
    return Promise.reject(error);
})

const request = axios;

export default request;