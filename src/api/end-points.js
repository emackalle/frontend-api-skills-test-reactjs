const api = {
    recipes: "/recipes",
    specials: "/specials",
}

export default api;