import api from "./end-points";
import request from "../utils/request";
import queryString from "query-string";

import alert from "../components/helpers/alert";

export async function getSpecials(params, config){
    try{
        let rs = await request.get(api.specials, { params: { ...params, ...config } });
        return { success: true, data: rs.data }
    }
    catch(e){
        await alert.error("error occured.");
        return { success: false };
    }
}

export async function updateSpecial({uuid, ...params}, config){
    try{
        let rs = await request.patch(`${api.specials}/${uuid}`, queryString.stringify(params), { ...config, headers: { "Content-Type": "application/x-www-form-urlencoded" } });
        return { success: true, data: rs.data }
    }
    catch(e){
        await alert.error("error occured.");
        return { success: false };
    }
}

export async function addSpecial(params, config){
    try{
        let rs = await request.post(api.specials, queryString.stringify(params), { ...config, headers: { "Content-Type": "application/x-www-form-urlencoded" } });
        return { success: true, data: rs.data }
    }
    catch(e){
        await alert.error("error occured.");
        return { success: false };
    }
}