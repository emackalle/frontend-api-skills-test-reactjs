import api from "./end-points";
import request from "../utils/request";
import alert from "../components/helpers/alert";

export async function getRecipes(params, config){
    try{
        let rs = await request.get(api.recipes, { params: { ...params, ...config } });
        return { success: true, data: rs.data }
    }
    catch(e){
        await alert.error("error occured.");
        return { success: false };
    }
}

export async function getRecipeDetails(uuid, config){
    try{
        let rs = await request.get(`${api.recipes}/${uuid}`, { params: config });
        return { success: true, data: rs.data }
    }
    catch(e){
        await alert.error("error occured.");
        return { success: false };
    }
}
