import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

import "./styles/default.scss";

const SweetAlert = withReactContent(Swal);

// For more configuration visit: https://sweetalert2.github.io/#usage

const defaultProps = {
  allowOutsideClick: false,  
  heightAuto: false,
}

export const custom = props => {
  let opts = typeof props === "object" ? { ...defaultProps, ...props } : props;
  return SweetAlert.fire(opts);
}

export const success = props => {
  let opts = typeof props === "string" ? { text: props } : props;
  return SweetAlert.fire({ type: "success", customClass: "swal2-alert-success",  ...defaultProps, ...opts });
}

export const error = props => {
  let opts = typeof props === "string" ? { text: props } : props;
  return SweetAlert.fire({ type: "error", customClass: "swal2-alert-error", ...defaultProps, ...opts });
}

export const info = props => {
  let opts = typeof props === "string" ? { text: props } : props;
  return SweetAlert.fire({ type: "info", customClass: "swal2-alert-info", ...defaultProps, ...opts });
}

export const warning = props => {
  let opts = typeof props === "string" ? { text: props } : props;
  return SweetAlert.fire({ type: "warning", customClass: "swal2-alert-warning", ...defaultProps, ...opts });
}

export const confirm = props => {
  let opts = typeof props === "string" ? { text: props } : props;
  return SweetAlert.fire({ type: "question", customClass: "swal2-alert-confirm", showCancelButton: true, ...defaultProps, ...opts });
}

export default { custom, success, error, info, warning, confirm };