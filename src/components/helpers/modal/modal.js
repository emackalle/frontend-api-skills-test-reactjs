import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Rodal from "rodal";

import "rodal/lib/rodal.css";
import "./styles/default.scss";

// HEADER
const Header = props => <header className={`modal-header`} {...props}>{props.children}</header>;

// CONTENT
const Content = props => <section className={`modal-content scroll-bar`} {...props}></section>;

// FOOTER
const Footer = props => <footer className={`modal-footer`} {...props}>{props.children}</footer>;

// CONTAINER
const Container = props => <>{props.children}</>;

// MODAL
const Modal = ({ render, children, className, visible, duration, ...rest }) => {
  // const [isVisible, setIsVisible] = useState(visible);
  // useEffect(() => {
  //   if(visible !== isVisible){
  //     if(visible){
  //       setIsVisible(visible);
  //     }else{
  //       window.setTimeout(() => setIsVisible(visible), duration);
  //     }  
  //   }
  // }, [visible])
  // if(!isVisible) return null;
  return(
    <Rodal visible={visible} duration={duration} className={classNames(`react-modal`, className)} { ...rest }>
      {render ? render({ Footer: Footer, Content: Content, Header: Header, Container: Container }) : children}
    </Rodal>
  )
}

// PROPTYPES
Modal.propTypes = {
   animation: PropTypes.oneOf(["door", "fade", "flip", "rotate", "slideDown", "slideLeft", "slideRight", "slideUp", "zoom"]),
   className: PropTypes.string,
   closeMaskOnClick: PropTypes.bool,
   closeOnEsc: PropTypes.bool,
   customMaskStyles: PropTypes.object,
   customStyles: PropTypes.object,
   duration: PropTypes.number,
   enterAnimation: PropTypes.string,
   height: PropTypes.number,
   leaveAnimation: PropTypes.string,
   measure: PropTypes.string,
   onAnimationEnd: PropTypes.func,
   onClose: PropTypes.func.isRequired,
   render: PropTypes.oneOfType([PropTypes.func, PropTypes.oneOf([null])]),
   showCloseButton: PropTypes.bool,
   showMask: PropTypes.bool,
   width: PropTypes.number,
   visible: PropTypes.bool,
}

// DEFAULT PROPS
Modal.defaultProps = {
   animation: "zoom",
   duration: 300,
   closeMaskOnClick: false,
   render: null,
}

export default Modal;
