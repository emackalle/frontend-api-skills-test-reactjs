import React from 'react';
import PropTypes from 'prop-types';

import config from "../../utils/config";

const Image = ({ src, alt }) => {
    if(!src) return <div className={`thumbnail no-img`}></div>
    const url = `${config.BASE_URL}${src}`;
    return <img src={url} alt={alt} />
}

export default Image;

Image.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
}