import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

const Button = ({ value, className, ...props }) => {
	return <button className={classNames(`btn btn-success`, className.split(" ").filter(x => x !== ""))} { ...props }>{value}</button>
}

Button.propTypes = {
	type: PropTypes.oneOf(['button', 'submit']),
	value: PropTypes.string.isRequired,
}

Button.defaultProps = {
	type: "button",
	className: "",
}

export default Button;
