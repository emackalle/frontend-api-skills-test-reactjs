import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withFormsy } from "formsy-react";

import { onChange, onBlur, onKeyPress, isValidInput } from "../common.js";

class TextArea extends Component {
   // RENDER
   render() {
      // RENDER DEFAULT
      return (
         <div className={classNames(`form-group field-text-area`, [...this.props.className.split(" ").filter(x => x !== ""), { error: !isValidInput(this.props) }])} >
            { this.props.label && <label htmlFor={this.props.id || this.props.name}>{this.props.label}</label> }
            <textarea
              className={classNames(`form-control`, { 'is-invalid': !isValidInput(this.props) })}
              disabled={this.props.disabled}
              form={this.props.form}
              id={this.props.id || this.props.name}
              onChange={e => onChange(this.props, e)}
              onBlur={e => onBlur(this.props)}
              onKeyPress={e => onKeyPress(this.props, e)}
              maxLength={this.props.maxLength}
              placeholder={this.props.placeholder}
              type={this.props.type}
              value={this.props.getValue()}
            />           
            { !isValidInput(this.props) && <div className={`invalid-feedback`}>{this.props.getErrorMessage()}</div> }
         </div>
     );
   }
 }

// PROP TYPES
TextArea.propTypes = {
   autoFocus: PropTypes.bool,
   disabled: PropTypes.bool,
   form: PropTypes.string,
   id: PropTypes.string,
   label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
   maxLength: PropTypes.number,
   name: PropTypes.string.isRequired,
   onChange: PropTypes.func,
   placeholder: PropTypes.string,
   readOnly: PropTypes.bool,
   required: PropTypes.bool,
   value: PropTypes.string,
}

// DEFAULT PROPS
TextArea.defaultProps = {
   autoFocus: false,
   className: "",
   disabled: false,
   id: null,
   label: null,
   maxLength: null,
   placeholder: null,
   readOnly: false,
   required: false,
}

export default withFormsy(TextArea);