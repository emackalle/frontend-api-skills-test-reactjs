import React, { useRef } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Formsy from "formsy-react";

const Form = ({ children, withWrap, className, ...rest }) => {
  return (
    <Formsy
      ref={useRef(`form`)}
      className={classNames([...className.split(" ").filter(x => x !== ""), { "form-wrap": withWrap }])}
      {...rest}
    >{children}</Formsy>
  )
}

// PROPTYPES
Form.propTypes = {
  autoComplete: PropTypes.string,
  name: PropTypes.string,
  onSubmit: PropTypes.func,
  onInvalidSubmit: PropTypes.func,
  onValidSubmit: PropTypes.func,
  withWrap: PropTypes.bool,
}
// DEFAULT PROPS
Form.defaultProps = {
  autoComplete: "off",
  className: "",
  withWrap: true,
}

export default Form;
