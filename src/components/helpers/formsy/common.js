// ONCHANGE
export function onChange(props, e){
	if(props.isPristine()){
		props.setValue(e.currentTarget.value, false);
	}else{
		props.setValue(e.currentTarget.value);
	}
	if(props.onChange) {
		props.onChange(e.currentTarget.value)
	}
}

// ONBLUR
export function onBlur(props){
	if(props.isPristine() || !props.isValidValue(props.getValue())){
		props.setValue(props.getValue());
	}         
}
	
// ONKEYDOWN
export function onKeyPress(props, e){
	if(e.which === 13){
		e.currentTarget.blur();
	}
}
	
// CHECK IF VALID INPUT
export function isValidInput(props){
	return props.isPristine() || props.isValidValue(props.getValue());
}	