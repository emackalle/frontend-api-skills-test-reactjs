import React from "react";
import PropTypes from "prop-types";

const Text = props => {
   const classNames = [`form-group`, ...props.className.split(" ").filter(x => x !== "")];
   return (
   <div className={ classNames.join(" ") } >
      <label>{ props.label }</label>
      <div className={`field-text`}><span>{ props.value }</span></div>
   </div>
  )
}

Text.defaultProps = {
  className: ""
}

Text.propTypes = {
  label: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.number.isRequired
  ]),
}

export default Text;
