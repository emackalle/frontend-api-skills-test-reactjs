import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

import Recipes from "./pages/recipes";
import RecipeDetails from "./pages/recipe-details";
import IngredientSpecials from './pages/ingredient-specials';

const Routes = () => {
    return(
        <Switch>
            <Route path={`/`} component={Recipes} exact />
            <Route path={`/recipes`} component={Recipes} exact />
            <Route path={`/recipes/:uuid`} component={RecipeDetails} exact />
            <Route path={`/recipes/:uuid/specials`} component={IngredientSpecials} exact />
            <Redirect to={`/`} />
        </Switch>
    )
}

export default Routes;