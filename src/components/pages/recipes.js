import React from 'react';

import { NavLink } from 'react-router-dom';

import { getRecipes } from "../../api/recipes-api";

import Image from "../helpers/image";

class Recipes extends React.Component{
    state = {
        recipes: [],    
    }
    async componentDidMount(){
        let rs = await getRecipes();
        if(rs.success){
            this.setState({
                ...this.state,
                recipes: rs.data,
            })
        }
    }
    render(){
        const { recipes } =  this.state;
        return(
            <div className={`container`}>
                <div className={`row recipes-list`}>
                    {recipes.map((recipe, key) => (
                        <div key={key} className={`col-4`}>
                            <div className={`card`}>
                                <Image src={recipe.images && recipe.images.medium} className={`card-img-top`} alt={`title`} />
                                <div className={`card-body`}>
                                    <h5 className={`card-title`}>{recipe.title}</h5>
                                    <p className={`card-text`}>{recipe.description}</p>
                                    <NavLink to={`/recipes/${recipe.uuid}`} className={`btn btn-primary`}>Read More</NavLink>
                                </div>
                            </div>   
                        </div>   
                    ))}          
                </div>  
            </div>
        )    
    }
}

export default Recipes;