import React from 'react';
import { NavLink } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';

import { Form, Input, Text, TextArea, Button } from "../helpers/formsy";
import Modal from "../helpers/modal";
import alert from "../helpers/alert";

import { getRecipeDetails } from "../../api/recipes-api";
import { getSpecials, updateSpecial, addSpecial } from "../../api/specials-api";

class IngredientSpecials extends React.Component{
    state = {
        recipeDetails: null,
        selectedIngredient: null,
        showModal: false,
		ingredientSpecials: [],
    }
    async componentDidMount(){
        let rs = await getRecipeDetails(this.props.match.params.uuid);
        if(rs.success){
            this.setState({ ...this.state, recipeDetails: rs.data });
            this.getIngredientSpecials(rs.data);
        }else{
            this.props.history.push("/recipes");
        }
    }
	async getIngredientSpecials({ ingredients }){
		let rs = await getSpecials({ ingredientId: ingredients.map(i => i.uuid) });
		if(rs.success){
			this.setState({ ...this.state, ingredientSpecials: rs.data });
		}
    }
    async onSaveSpecial(data){
        let rs = await updateSpecial(data);
        if(rs.success){
            await alert.success("Saved.");
            this.setState({ ...this.state, selectedIngredient: null, showModal: false });
            this.getIngredientSpecials(this.state.recipeDetails);
        }
    }
    async onAddSpecial(data){
        let rs = await addSpecial(data);
        if(rs.success){
            await alert.success("Added.");
            this.setState({ ...this.state, selectedIngredient: null, showModal: false });
            this.getIngredientSpecials(this.state.recipeDetails);
        }
    }
    onSubmitSpecial(data){
        const { selectedIngredient } = this.state;
        if(selectedIngredient.action === "add"){
            return this.onAddSpecial({ ingredientId: selectedIngredient.uuid, ...data });
        }
        return this.onSaveSpecial({ uuid: selectedIngredient.special.uuid, ...data });
    }

    toggleModal(selectedIngredient=null){
        this.setState({
            ...this.state,
            showModal: !this.state.showModal,
            selectedIngredient,
        })
    }
    render(){
        const { recipeDetails, ingredientSpecials } = this.state;
        return (
            <div className={`container`}>  
                <h3 className={`section-title`}>{ recipeDetails ? `${recipeDetails.title} Ingredient Specials` : `` }</h3>
                <Form>
                    { recipeDetails && recipeDetails.ingredients.map((ingredient, key) => {
                        const special = ingredientSpecials.find(x => x.ingredientId === ingredient.uuid);
                        return (
                            <div className={`list-item`} key={key}>
                                <div className={`form-row`}>
                                    <Text name={`name`} label={`Name`} className={`col-md-4`} value={ingredient.name} disabled />
                                    <Text name={`type`} label={`Type`} className={`col-md-4`} value={special ? special.type : `none`} />
                                    <Text name={`title`} label={`Title`} className={`col-md-4`} value={special ? special.title : `none`} />
                                </div>
                                <Text name={`text`} label={`Text`} value={special ? ReactHtmlParser(special.text) : `none`} />   
                                {special ? (
                                    <button className={`btn btn-primary`} onClick={() => this.toggleModal({ ...ingredient, action: 'update', special })}>Update</button>
                                ) : (
                                    <button className={`btn btn-primary`} onClick={() => this.toggleModal({ ...ingredient, action: 'add', special })}>Add</button>
                                )}               
                            </div>
                        )
                    })}                    
                    <div className={`form-group action-btns`}>
                        <div className={`btns`}>
                            <NavLink to={`/recipes/${this.props.match.params.uuid}`} className={`btn btn-secondary`}>Back</NavLink>
                        </div>
                    </div>   
                </Form>
                <ModalForm state={this.state} actions={{ toggleModal: this.toggleModal.bind(this), onSubmitSpecial: this.onSubmitSpecial.bind(this) }} />
            </div>
        )    
    }
}

const ModalForm = ({ state, actions }) => {
    const { selectedIngredient, showModal } = state;
    const { toggleModal, onSubmitSpecial } = actions;
    if(!showModal || !selectedIngredient) return null;
    return(      
        <Modal visible={showModal} onClose={toggleModal} height={500} width={700}>
            <Form onValidSubmit={onSubmitSpecial}>
                <h3 className={`section-title`}>{selectedIngredient.name}</h3>
                <div className={`form-row`}>
                    <Input name={`type`} label={`Type`} className={`col-md-6`} value={selectedIngredient.special ? selectedIngredient.special.type : ``} />
                    <Input name={`title`} label={`Title`} className={`col-md-6`} value={selectedIngredient.special ? selectedIngredient.special.title : ``} />
                </div>
                <TextArea name={`text`} label={`Text (Optional)`} value={selectedIngredient.special ? selectedIngredient.special.text : ``} />
                <div className={`form-group action-btns`}>
                    <div className={`btns`}>
                        <Button type={`submit`} value={selectedIngredient.action === "add" ? "Add" : "Save"} />
                        <button className={`btn btn-danger`} onClick={toggleModal}>Cancel</button>
                    </div>
                </div>   
            </Form>       
        </Modal> 
    )
}

export default IngredientSpecials;