import React from 'react';
import { NavLink } from 'react-router-dom';

import ReactHtmlParser from 'react-html-parser';

import { getRecipeDetails } from "../../api/recipes-api";
import { getSpecials } from "../../api/specials-api";

import Image from "../helpers/image";

class RecipeDetails extends React.Component{
	state = {
		recipeDetails: null,
		ingredientSpecials: [],
	}
	async componentDidMount(){
		let rs = await getRecipeDetails(this.props.match.params.uuid);
		if(rs.success){
			this.setState({ ...this.state, recipeDetails: rs.data });
			this.getIngredientSpecials(rs.data.ingredients);
		}else{
			this.props.history.push("/recipes");
		}
	}
	async getIngredientSpecials(ingredients){
		let rs = await getSpecials({ ingredientId: ingredients.map(i => i.uuid) });
		if(rs.success){
			this.setState({ ...this.state, ingredientSpecials: rs.data });
		}
	}
	render(){
		const { recipeDetails, ingredientSpecials } = this.state;
		return (
			<div className={`container page-view`}>  
				<div className={`row recipe-img`}>
					{recipeDetails && (
						<Image src={recipeDetails.images && recipeDetails.images.medium} alt={`recipe-img`} />
					)}
				</div>

				<div className={`row recipe-content`}>
					<div className={`col-5 recipe-intro`}>
						<div className={`row recipe-title`}>
							<div className={`col`}>
								<h1>{recipeDetails ? recipeDetails.title : "-"}</h1>
							</div>
						</div>
						<div className={`row recipe-desc`}>
							<div className={`col`}>
								<p>{recipeDetails ? recipeDetails.description : "-"}</p>
							</div>
						</div>
						<div className={`row recipe-summary`}>
							<div className={`col-3`}>
								<label>Servings</label>
								<div>{recipeDetails ? recipeDetails.servings : "-"}</div>
							</div>
							<div className={`col-3`}>
								<label>Prep. Time</label>
								<div>{recipeDetails ? recipeDetails.prepTime : "-"}</div>
							</div>
							<div className={`col-3`}>
								<label>Cook Time</label>
								<div>{recipeDetails ? recipeDetails.cookTime : "-"}</div>
							</div>
						</div>						
					</div>

					<div className={`col-7 recipe-instructions`}>
						<div className={`row`}>
							<div className={`col`}>
								<label>Ingredients</label>
								<ul>
									{recipeDetails && recipeDetails.ingredients.map((ingredient, key) => {
										const specials = ingredientSpecials.find(special => special.ingredientId === ingredient.uuid);
										return(
											<li key={key}>
												{`${ingredient.amount} ${ingredient.measurement} of ${ingredient.name}`}
												{specials && (
													<>
														<div><strong>Specials</strong></div>
														<ul>
															<li>{specials.title}</li>
															<li>{specials.type}</li>
															{specials.text && <li>{ReactHtmlParser(specials.text)}</li>}
														</ul>
													</>
												)}
											</li>
										)
									})}
								</ul>
							</div>
						</div>
						<div className={`row`}>
							<div className={`col`}>
								<label>Directions</label>
								<ul>
									{recipeDetails && recipeDetails.directions.map((details, key) => (
										<li key={key}>{details.instructions} {details.optional && (<i>(Optional)</i>)}</li>
									))}
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div className={`row action-btns`}>
					<div className={`col-auto mr-auto`}>
						<div className={`row`}>
							<div className={`col`}>
								<label>Post Date</label>
								<div>{recipeDetails ? recipeDetails.postDate : "-"}</div>
							</div>
							<div className={`col`}>
								<label>Edit Date</label>
								<div>{recipeDetails ? recipeDetails.editDate : "-"}</div>
							</div> 
						</div>
					</div>
					<div className={`col-auto btns`}>
						<NavLink to={`/recipes/${recipeDetails && recipeDetails.uuid}/specials`} className={`btn btn-primary`}>Specials</NavLink>
						<NavLink to={`/recipes`} className={`btn btn-secondary`}>Back</NavLink>
					</div>
				</div>
			</div>
		)    
	}
}

export default RecipeDetails;